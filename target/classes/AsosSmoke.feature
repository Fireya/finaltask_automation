Feature: Main functionality tests
  As a user
  I want to test interactions with objects on site
  So that I can be sure that interactions with objects on the site works correctly

  Scenario Outline: Check interactions with main objects
    Given User opens '<homePage>' page
    And User checks visibility of header
    And User checks visibility of main block
    And User checks visibility of footer
    And User checks visibility of search field
    And User checks visibility of account button
    And User moves pointer to the account dropdown
    And User checks visibility of Join button
    And User checks visibility of SignIn button
    And User checks visibility of wishlist button
    And User checks visibility of cart button
    When User clicks on Women category button
    And User checks name of the '<expectedUrlOfWomen>' link
    Then User checks dropdowns of Women subcategories
    And User clicks on Men category button
    And User checks name of the '<expectedUrlOfMen>' link
    And User checks dropdowns of Men subcategories

    Examples:
      |homePage             |expectedUrlOfWomen|expectedUrlOfMen|
      |https://www.asos.com/|/women/           |/men/           |

  Scenario Outline: Check social media links in footer
    Given User opens '<homePage>' page
    And User checks visibility of header
    And User checks visibility of main block
    And User checks visibility of footer
    When User clicks facebook link
    And User checks name of the '<expectedUrlOfFacebook>' link
    And User clicks instagram link
    And User checks name of the '<expectedUrlOfInstagram>' link
    And User clicks snapchat link
    And User checks name of the '<expectedUrlOfSnapchat>' link

    Examples:
      |homePage             |expectedUrlOfFacebook         |expectedUrlOfInstagram         |expectedUrlOfSnapchat                   |
      |https://www.asos.com/|https://www.facebook.com/ASOS/|https://www.instagram.com/asos/|https://www.snapchat.com/add/asosfashion|

  Scenario Outline: Check preferences change
    Given User opens '<homePage>' page
    And User checks visibility of header
    When User clicks on Change preferences button
    And User checks visibility of Preferences popup
    And User clicks and chooses country '<country>' to shop in
    And User clicks Save country button
    And User checks visibility of header
    And User clicks on Change preferences button
    And User checks visibility of Preferences popup
    Then User checks and compares value in field with '<expectedCountry>' expected country

    Examples:
      |homePage             |country|expectedCountry|
      |https://www.asos.com/|CH     |Switzerland    |
      |https://www.asos.com/|BY     |Беларусь       |
      |https://www.asos.com/|CN     |China          |

  Scenario Outline: Check currency change
    Given User opens '<homePage>' page
    And User checks visibility of header
    When User clicks on Change preferences button
    And User checks visibility of Preferences popup
    And User clicks and chooses country '<country>' to shop in
    And User clicks Save country button
    And User checks visibility of header
    And User clicks on Women category button
    And User moves pointer to the Shoes button
    And User clicks on View All shoes category button
    Then User checks changed currency '<expectedCurrency>'

    Examples:
      |homePage             |country|expectedCurrency|
      |https://www.asos.com/|CH     |CHF             |
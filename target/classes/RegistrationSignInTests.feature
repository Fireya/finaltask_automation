Feature: Registration and Sign In Tests for Asos
  As a user
  I want to test registration and sign in site functionality
  So that I can be sure that site registration and sign in functionality works correctly

  Scenario Outline: Check registration with incorrect parameters
    Given User opens '<homePage>' page
    And User checks visibility of header
    When User moves pointer to the account dropdown
    And User checks visibility of Join button
    And User clicks on Join button
    And User checks name of the '<expectedLink>' link
    Then User inputs invalid '<email>' text to Email field on Join page and checks inputed value
    And User inputs invalid '<firstName>' text to First name field on Join page and checks inputed value
    And User inputs invalid '<lastName>' text to Last name field on Join page and checks inputed value
    And User inputs invalid '<password>' text to Password field on Join page and checks inputed value

    Examples:
      |homePage             |expectedLink                         |email   |firstName|lastName  |password  |
      |https://www.asos.com/|https://my.asos.com/identity/register|        |         |          |          |
      |https://www.asos.com/|https://my.asos.com/identity/register|123     |"&<>     |"&<>      |0123      |

  Scenario Outline: Check sign in with incorrect parameters
    Given User opens '<homePage>' page
    And User checks visibility of header
    When User moves pointer to the account dropdown
    And User checks visibility of SignIn button
    And User clicks on SignIn button
    And User checks name of the '<expectedLink>' link
    Then User inputs incorrect '<email>' text to Email field
    And User inputs incorrect '<password>' text to Password field
    And User clicks on SignIn button on SignIn page
    And User checks inputed values

    Examples:
      |homePage             |expectedLink                      |email                            |password    |
      |https://www.asos.com/|https://my.asos.com/identity/login|                                 |            |

package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;
import pages.*;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.junit.Assert.*;

public class DefinitionSteps {
    private static final long DEFAULT_TIMEOUT = 30;
    WebDriver driver;
    PageFactoryManager pageFactoryManager;
    HomePage homePage;
    RegistrationSignInPage registrationSignInPage;
    SearchResultsPage searchResultsPage;
    ProductPage productPage;
    WishlistPage wishlistPage;
    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
        homePage = pageFactoryManager.getHomePage();
        registrationSignInPage = pageFactoryManager.getRegistrationSignInPage();
        searchResultsPage = pageFactoryManager.getSearchResultsPage();
        productPage = pageFactoryManager.getProductPage();
        wishlistPage = pageFactoryManager.getWishlistPage();
    }
    @After
    public void tearDown() {
        driver.quit();
    }
    @And("User opens {string} page")
    public void openStartPage(final String urlOfPage)
    {
        driver.get(urlOfPage);
    }
    @And("User checks visibility of header")
    public void userChecksVisibilityOfHeader() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.checkHeaderVisibility());
    }
    @And("User checks visibility of main block")
    public void userChecksVisibilityOfMainBlock() {
        assertTrue(homePage.checkMainBlockVisibility());
    }
    @And("User checks visibility of footer")
    public void userChecksVisibilityOfFooter() {
        assertTrue(homePage.checkFooterVisibility());
    }
    @And("User checks visibility of search field")
    public void userChecksVisibilityOfSearchField() {
        assertTrue(homePage.checkSearchFieldVisibility());
    }
    @And("User checks visibility of account button")
    public void userChecksVisibilityOfAccountButton() {
        assertTrue(homePage.checkAccountDropdownVisibility());
    }
    @And("User checks visibility of wishlist button")
    public void userChecksVisibilityOfWishlistButton() {

    }
    @And("User checks visibility of cart button")
    public void userChecksVisibilityOfCartButton() {
    }
    @And("User moves pointer to the account dropdown")
    public void userMovesPointerToTheAccountDropdown() {
        homePage.moveOverElement(homePage.getAccountDropdown());
    }
    @And("User checks visibility of Sign Out button")
    public void userChecksVisibilityOfSignOutButton() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getSignOutButton());
        assertTrue(homePage.checkSignOutButtonVisibility());
    }
    @And("User clicks Sign Out button")
    public void userClicksSignOutButton() {
        homePage.clickSignOutButton();
    }
    @And("User checks visibility of Join button")
    public void userChecksVisibilityOfJoinButton() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getJoinButton());
        assertTrue(homePage.checkJoinButtonVisibility());
    }
    @And("User clicks on Join button")
    public void userClicksOnJoinButton() {
        homePage.clickJoinButton();
    }
    @And("User checks name of the {string} link")
    public void userChecksNameOfTheLink(final String expectedLink) {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(driver.getCurrentUrl().contains(expectedLink));
    }
    @And("User inputs invalid {string} text to Email field on Join page and checks inputed value")
    public void userInputsInvalidEmailTextToEmailField(final String email) {
        registrationSignInPage.getEmailFieldOnJoinPage().sendKeys(email);
        registrationSignInPage.clickAtElementByOffSet(-30, 0);
        assertTrue(registrationSignInPage.isEmailErrorMessageOnJoinPageVisible());
    }
    @And("User inputs invalid {string} text to First name field on Join page and checks inputed value")
    public void userInputsFirstNameTextToFirstNameFieldAndChecksInputedValue(final String firstName) {
        registrationSignInPage.getFirstNameField().sendKeys(firstName);
        registrationSignInPage.clickAtElementByOffSet(-30, 0);
        assertTrue(registrationSignInPage.isFirstNameErrorMessageOnJoinPageVisible());
    }
    @And("User inputs invalid {string} text to Last name field on Join page and checks inputed value")
    public void userInputsLastNameTextToLastNameFieldAndChecksInputedValue(final String lastName) {
        registrationSignInPage.getLastNameField().sendKeys(lastName);
        registrationSignInPage.clickAtElementByOffSet(-30, 0);
        assertTrue(registrationSignInPage.isLastNameErrorMessageOnJoinPageVisible());
    }
    @And("User inputs invalid {string} text to Password field on Join page and checks inputed value")
    public void userInputsPasswordTextToPasswordFieldAndChecksInputedValue(final String password) {
        registrationSignInPage.getPasswordField().sendKeys(password);
        registrationSignInPage.clickAtElementByOffSet(-30, 0);
        assertTrue(registrationSignInPage.isPasswordErrorMessageOnJoinPageVisible());
    }
    @And("User checks visibility of SignIn button")
    public void userChecksVisibilityOfSignInButton() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getSignInButton());
        assertTrue(homePage.checkSignInButtonVisibility());
    }
    @And("User clicks on SignIn button")
    public void userClicksOnSignInButton() {
        homePage.clickSignInButton();
    }
    @And("User inputs {string} text to Email field and checks inputed value")
    public void userInputsEmailTextToEmailField(final String email) {
        registrationSignInPage.getEmailFieldOnSignInPage().sendKeys(email);
        assertEquals(registrationSignInPage.getAttribute(registrationSignInPage.getEmailFieldOnSignInPage(), "value"), email);
    }
    @And("User inputs {string} text to Password field and checks inputed value")
    public void userPastesPasswordTextToPasswordField(final String password) {
        registrationSignInPage.getPasswordField().sendKeys(password);
        assertEquals(registrationSignInPage.getAttribute(registrationSignInPage.getPasswordField(), "value"), password);
    }
    @And("User clicks on SignIn button on SignIn page")
    public void userClicksOnSignInButtonOnSignInPage() {
        registrationSignInPage.clickSignInButton();
    }
    @And("User checks visibility of name of account")
    public void userChecksVisibilityOfNameOfAccount() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getNameOfAccountOnTheAccountDropdown());
        assertTrue(homePage.checkNameOfAccountVisibility());
    }
    @And("User checks that user name equals {string}")
    public void userChecksThatUserNameEqualsFirstName(final String expectedName) {
        assertTrue(homePage.getNameOfAccountOnTheAccountDropdown().getAttribute("innerText").contains(expectedName));
    }
    @And("User inputs incorrect {string} text to Email field")
    public void userInputsIncorrectEmailTextToEmailFieldAndChecksInputedValue(final String email) {
        registrationSignInPage.getEmailFieldOnSignInPage().sendKeys(email);
    }
    @And("User inputs incorrect {string} text to Password field")
    public void userInputsIncorrectPasswordTextToPasswordFieldAndChecksInputedValue(final String password) {
        registrationSignInPage.getPasswordField().sendKeys(password);
    }
    @And("User checks inputed values")
    public void userChecksInputedValuesEmailPassword() {
        assertTrue(registrationSignInPage.isEmailErrorMessageOnSignInPageVisible());
        assertTrue(registrationSignInPage.isPasswordErrorMessageOnJoinPageVisible());
    }
    @And("User clicks on Women category button")
    public void userClicksOnWomenCategoryButton() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getWomenCategoryButton());
        assertTrue(homePage.checkWomenCategoryButtonVisibility());
        homePage.clickWomenCategoryButton();
    }
    @And("User moves pointer to the Shoes button")
    public void userMovesPointerToTheShoesButton() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.moveOverElement(homePage.getShoesCategoryButton());
    }
    @And("User clicks on View All shoes category button")
    public void userClicksOnViewAllShoesCategoryButton() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getViewAllShoesCategory());
        assertTrue(homePage.checkViewAllShoesCategoryButton());
        homePage.clickViewAllShoesCategory();
    }
    @And("User adds {int} product to the cart")
    public void userAddsNumberOfProductToTheCart(final int numberOfProducts) {
        Select dropdown;
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        for(int i = 0; i < numberOfProducts; i++)
        {
            searchResultsPage.clickOnProduct(i);
            productPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
            for(WebElement element : productPage.getListOfParametersToSelect())
            {
                dropdown = new Select(element);
                dropdown.selectByIndex(2);
            }
            productPage.clickAddToCartButton();
            productPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
            productPage.clickShoesBreadcrumb();
            searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        }
    }
    @And("User checks number of items in cart and compares to {string}")
    public void userChecksNumberOfItemsInCartAndComparesToNumberOfProduct(final String expectedNumber) {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getNumberOfProductsInCart());
        assertTrue(homePage.checkNumberOfProductsInCartVisibility());
        assertEquals(expectedNumber, homePage.getAttribute(homePage.getNumberOfProductsInCart(), "innerText"));
    }
    @And("User moves pointer to the cart dropdown")
    public void userMovesPointerToTheCartDropdown() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getCartDropdown());
        assertTrue(homePage.checkCartDropdownVisibility());
        homePage.moveOverElement(homePage.getCartDropdown());
    }
    @And("User clears up a cart and checks number of items in cart")
    public void userClearsUpACart() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getCartDropdownPopUp());
        assertTrue(homePage.checkCartDropdownPopUpVisibility());
        for (int i = 0; i < homePage.getDeleteItemsFromCartButtonsList().size(); i++)
        {
            homePage.clickDeleteItemsFromCartButton(i);
            homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        }
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getEmptyCartButton());
        assertTrue(homePage.checkEmptyCartButtonVisibility());
        assertTrue(homePage.getAttribute(homePage.getEmptyCartButton(), "innerText").isEmpty());
    }
    @And("User  inputs {string} text to Search field")
    public void userInputsTextToSearchField(final String searchText) {
        homePage.inputTextToSearchField(searchText);
    }
    @And("User checks that url contains search {string} text")
    public void userChecksThatUrlContainsSearchSearchTextText(final String searchText) {
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        searchResultsPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertTrue(searchResultsPage.getCurrentUrl().contains(searchText));
    }
    @And("User checks that products contains search {string} text")
    public void userChecksThatProductsContainsSearchText(final String searchText) {
        String[] words = searchText.split("\\s");
        for (WebElement element: searchResultsPage.getProductNamesList()) {
            int trueAsserts = 0;
            for(String str : words)
            {
                if(searchResultsPage.getAttribute(element, "innerText").contains(str))
                {
                    trueAsserts++;
                }
            }
            if(trueAsserts > 0) assertTrue(true);
        }
    }
    @And("User checks visibility of Brand and Price filters")
    public void userChecksVisibilityOfBrandandPriceFilters() {
        searchResultsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, searchResultsPage.getBrandFilter());
        assertTrue(searchResultsPage.checkBrandFilterVisibility());
        searchResultsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, searchResultsPage.getSortFilter());
        assertTrue(searchResultsPage.checkSortFilterVisibility());
    }
    @And("User clicks and chooses Brand {string} and Price filters parameters")
    public void userClicksAndChoosesBrandBrandAndPriceLowPriceHighPriceFiltersParameters(final String brand) {
        searchResultsPage.clickOnBrandFilter();
        searchResultsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, searchResultsPage.getBrandFilterDropdown());
        assertTrue(searchResultsPage.checkBrandFilterDropdownVisibility());
        for(WebElement element : searchResultsPage.getBrandsList())
        {
            if(searchResultsPage.getAttribute(element, "innerText").contains(brand))
            {
                element.click();
            }
            searchResultsPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        }
        searchResultsPage.clickOnSortFilter();
        searchResultsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, searchResultsPage.getSortFilterDropdown());
        assertTrue(searchResultsPage.checkSortFilterDropdownVisibility());
        searchResultsPage.clickSortLowToHighFilter();
    }
    @And("User checks that products corresponds to brand {string} name and in low to high order")
    public void userChecksThatProductsCorrespondsToBrandBrandNameAndInLowToHighOrder(final String brand) throws InterruptedException {
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        searchResultsPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        Thread.sleep(10000);
        for (WebElement element: searchResultsPage.getProductNamesList()) {
            assertTrue(searchResultsPage.getAttribute(element, "innerText").contains(brand));
        }
        for (int i = 0; i < searchResultsPage.getProductPriceList().size()-1; i++)
        {
            assertTrue(Double.parseDouble(searchResultsPage.getAttribute(searchResultsPage.
                            getProductPriceList().get(i), "innerText").
                    substring(1)) <= Double.parseDouble(searchResultsPage.
                    getAttribute(searchResultsPage.
                            getProductPriceList().get(i+1), "innerText").substring(1)));
        }
    }
    @And("User adds {string} product to the wishlist")
    public void userAddsProductToTheWishlist(final String numberOfProducts) {
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        for(int i = 0; i < Integer.parseInt(numberOfProducts); i++)
        {
            searchResultsPage.clickOnProductAddToWishlist(i);
            searchResultsPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        }
    }
    @And("User clicks on Wishlist button")
    public void userClicksOnWishlistButton() {
        homePage.clickWishlistButton();
    }
    @And("User checks quantity {string} of products in wishlist")
    public void userChecksQuantityOfProductsInWishlist(final String numberOfProducts) {
        wishlistPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, wishlistPage.getItemsCount());
        assertTrue(wishlistPage.checkItemsCountVisible());
        assertEquals(numberOfProducts, wishlistPage.getAttribute(wishlistPage.getItemsCount(), "innerText").substring(0, 1));
    }
    @And("User checks dropdowns of Women subcategories")
    public void userChecksDropdownsOfWomenSubcategories() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        for(int i = 0; i < homePage.getWomenSubcategoriesList().size(); i++)
        {
            homePage.moveOverElement(homePage.getWomenSubcategoriesList().get(i));
            homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getWomenSubcategoriesDropdownsList().get(i));
            assertTrue(homePage.getWomenSubcategoriesDropdownsList().get(i).isDisplayed());
        }
    }
    @And("User clicks on Men category button")
    public void userClicksOnMenCategoryButton() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getMenCategoryButton());
        assertTrue(homePage.checkMenCategoryButtonVisibility());
        homePage.clickMenCategoryButton();
    }
    @And("User checks dropdowns of Men subcategories")
    public void userChecksDropdownsOfMenSubcategories() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        for(int i = 0; i < homePage.getMenSubcategoriesList().size(); i++)
        {
            homePage.moveOverElement(homePage.getMenSubcategoriesList().get(i));
            homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getMenSubcategoriesDropdownsList().get(i));
            assertTrue(homePage.getMenSubcategoriesDropdownsList().get(i).isDisplayed());
        }
    }
    @And("User clicks facebook link")
    public void userClicksFacebookLink() {
        homePage.clickFacebookLink();
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.switchTab(homePage.getTabs(), 1);
    }
    @And("User clicks instagram link")
    public void userClicksInstagramLink() {
        driver.close();
        homePage.switchTab(homePage.getTabs(), 0);
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getInstagramLink());
        assertTrue(homePage.checkInstagramLinkVisibility());
        homePage.clickInstagramLink();
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.switchTab(homePage.getTabs(), 1);
    }
    @And("User clicks snapchat link")
    public void userClicksSnapchatLink() {
        driver.close();
        homePage.switchTab(homePage.getTabs(), 0);
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getSnapchatLink());
        assertTrue(homePage.checkSnapchatLinkVisibility());
        homePage.clickSnapchatLink();
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.switchTab(homePage.getTabs(), 1);
    }
    @And("User clicks on Change preferences button")
    public void userClicksOnChangePreferencesButton() {
        homePage.clickChangePreferencesButton();
    }
    @And("User checks visibility of Preferences popup")
    public void userChecksVisibilityOfPreferencesPopup() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getPreferencesPopup());
        assertTrue(homePage.checkPreferencesPopupVisibility());
    }
    @And("User clicks and chooses country {string} to shop in")
    public void userClicksAndChoosesCountryStringToShopIn(final String country) {
        Select dropdown;
        dropdown = new Select(homePage.getShopInDropdown());
        dropdown.selectByValue(country);
    }
    @And("User clicks Save country button")
    public void userClicksSaveCountryButton() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getSaveCountryButton());
        homePage.clickSaveCountryButton();
    }
    @And("User checks and compares value in field with {string} expected country")
    public void userChecksAndComparesValueInFieldWithCountryCountry(final String expectedCountry) {
        Select dropdown;
        dropdown = new Select(homePage.getShopInDropdown());
        assertEquals(expectedCountry, dropdown.getFirstSelectedOption().getAttribute("innerText"));
    }
    @And("User checks changed currency {string}")
    public void userChecksChangedCurrencyExpectedCurrency(final String expectedCurrency) {
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        for (int i = 0; i < searchResultsPage.getProductPriceList().size(); i++)
        {
            assertEquals(expectedCurrency, searchResultsPage.getAttribute(searchResultsPage.
                            getProductPriceList().get(i), "innerText").substring(0, 3));
        }
    }
}
package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultsPage extends BasePage {
    @FindBy(xpath = "//article[@data-auto-id]//a[@href]")
    private List<WebElement> productsList;
    @FindBy(xpath = "//a//div[@data-auto-id]//p")
    private List<WebElement> productNamesList;
    @FindBy(xpath = "//a[contains(@aria-label, ' Price')]//span[@data-auto-id='productTilePrice'] | //span[@data-auto-id='productTileSaleAmount']")
    private List<WebElement> productPriceList;
    @FindBy(xpath = "//li[@data-dropdown-id='brand']//button[@aria-haspopup]")
    private WebElement brandFilter;
    @FindBy(xpath = "//li[@data-dropdown-id='sort']//button[@aria-haspopup]")
    private WebElement sortFilter;
    @FindBy(xpath = "//li[@data-dropdown-id='sort']//li[contains(@id, 'low_to_high')]")
    private WebElement sortLowToHighFilter;
    @FindBy(xpath = "//li[@data-dropdown-id='brand']//label[@for]/div")
    private List<WebElement> brandsList;
    @FindBy(xpath = "//li[@data-dropdown-id='brand']//div[@data-filter-dropdown]")
    private WebElement brandFilterDropdown;
    @FindBy(xpath = "//li[@data-dropdown-id='sort']//div[@data-filter-dropdown]")
    private WebElement sortFilterDropdown;
    @FindBy(xpath = "//button[@data-auto-id='saveForLater']")
    private List<WebElement> productAddToWishlistList;
    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }
    public boolean checkBrandFilterVisibility() {
        return brandFilter.isDisplayed();
    }
    public boolean checkSortFilterVisibility() {
        return sortFilter.isDisplayed();
    }
    public boolean checkSortFilterDropdownVisibility() {
        return sortFilterDropdown.isDisplayed();
    }
    public boolean checkBrandFilterDropdownVisibility() {
        return brandFilterDropdown.isDisplayed();
    }
    public List<WebElement> getProductNamesList() {
        return productNamesList;
    }
    public List<WebElement> getProductPriceList() {
        return productPriceList;
    }
    public List<WebElement> getBrandsList() {
        return brandsList;
    }
    public WebElement getBrandFilter() {
        return brandFilter;
    }
    public WebElement getSortFilter() {
        return sortFilter;
    }
    public WebElement getBrandFilterDropdown() {
        return brandFilterDropdown;
    }
    public WebElement getSortFilterDropdown() {
        return sortFilterDropdown;
    }
    public void clickOnProduct(int index) {
        productsList.get(index).click();
    }
    public void clickOnProductAddToWishlist(int index) {
        productAddToWishlistList.get(index).click();
    }
    public void clickOnBrandFilter() {
        brandFilter.click();
    }
    public void clickOnSortFilter() {
        sortFilter.click();
    }
    public void clickSortLowToHighFilter() {
        sortLowToHighFilter.click();
    }
}

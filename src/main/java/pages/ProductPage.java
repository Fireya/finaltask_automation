package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ProductPage extends BasePage {
    @FindBy(xpath = "//div[@class='colour-size-select' and not(@style)]//select")
    private List<WebElement> listOfParametersToSelect;
    @FindBy(xpath = "//button[@id='product-add-button']")
    private WebElement addToCartButton;
    @FindBy(xpath = "//nav[@aria-label='breadcrumbs']//a[text()='Shoes']")
    private WebElement shoesBreadcrumb;
    public ProductPage(WebDriver driver) {
        super(driver);
    }
    public List<WebElement> getListOfParametersToSelect() {
        return listOfParametersToSelect;
    }
    public void clickAddToCartButton() {
        addToCartButton.click();
    }
    public void clickShoesBreadcrumb() {
        shoesBreadcrumb.click();
    }
}

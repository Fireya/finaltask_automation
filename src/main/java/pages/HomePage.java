package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class HomePage extends BasePage {
    @FindBy(xpath = "//header")
    private WebElement header;
    @FindBy(xpath = "//main")
    private WebElement mainBlock;
    @FindBy(xpath = "//footer")
    private WebElement footer;
    @FindBy(xpath = "//button[@aria-label='My Account']")
    private WebElement accountDropdown;
    @FindBy(xpath = "//a[@data-testid='signup-link']")
    private WebElement joinButton;
    @FindBy(xpath = "//a[@data-testid='signin-link']")
    private WebElement signInButton;
    @FindBy(xpath = "//div[@id='myaccount-dropdown']//span[text()]")
    private WebElement nameOfAccountOnTheAccountDropdown;
    @FindBy(xpath = "//a[@id='women-floor']")
    private WebElement womenCategoryButton;
    @FindBy(xpath = "//a[@id='men-floor']")
    private WebElement menCategoryButton;
    @FindBy(xpath = "//nav[contains(@aria-label, 'Women') and not(@aria-hidden)]//button[@data-testid='primarynav-button']//span[text()='Shoes']")
    private WebElement shoesCategoryButton;
    @FindBy(xpath = "//nav[contains(@aria-label,'Women') and not (@aria-hidden)]//div[@data-testid='secondarynav-container']")
    private List<WebElement> womenSubcategoriesDropdownsList;
    @FindBy(xpath = "//nav[contains(@aria-label,'Men') and not (@aria-hidden)]//div[@data-testid='secondarynav-container']")
    private List<WebElement> menSubcategoriesDropdownsList;
    @FindBy(xpath = "//nav[contains(@aria-label,'Women') and not (@aria-hidden)]//button[@data-testid='primarynav-button']")
    private List<WebElement> womenSubcategoriesList;
    @FindBy(xpath = "//nav[contains(@aria-label,'Men') and not (@aria-hidden)]//button[@data-testid='primarynav-button']")
    private List<WebElement> menSubcategoriesList;
    @FindBy(xpath = "//a[contains(@href,'facebook')]")
    private WebElement facebookLink;
    @FindBy(xpath = "//a[contains(@href,'instagram')]")
    private WebElement instagramLink;
    @FindBy(xpath = "//a[contains(@href,'snapchat')]")
    private WebElement snapchatLink;
    @FindBy(xpath = "//nav[contains(@aria-label, 'Women') and not(@aria-hidden)]//a[contains(@href, 'view+all') and contains(@href, 'shoes')]")
    private WebElement viewAllShoesCategory;
    @FindBy(xpath = "//button[@data-testid='signout-link']")
    private WebElement signOutButton;
    @FindBy(xpath = "//button[@aria-controls='minibag-dropdown']//span[not(@type)]")
    private WebElement numberOfProductsInCart;
    @FindBy(xpath = "//button[@aria-controls='minibag-dropdown']")
    private WebElement cartDropdown;
    @FindBy(xpath = "//div[@id='minibag-dropdown']//button[@aria-label='Delete this item']")
    private List<WebElement> deleteItemsFromCartButtonsList;
    @FindBy(xpath = "//div[@data-testid='minibag-dropdown']")
    private WebElement cartDropdownPopUp;
    @FindBy(xpath = "//a[@aria-label='Bag 0 items']//span")
    private WebElement emptyCartButton;
    @FindBy(xpath = "//input[@data-testid='search-input']")
    private WebElement searchField;
    @FindBy(xpath = "//a[@aria-label='Saved Items']")
    private WebElement wishlistButton;
    @FindBy(xpath = "//header//button[@data-testid='country-selector-btn']")
    private WebElement changePreferencesButton;
    @FindBy(xpath = "//section[@data-testid='country-selector-form']")
    private WebElement preferencesPopup;
    @FindBy(xpath = "//section[@data-testid='country-selector-form']//select[@id='country']")
    private WebElement shopInDropdown;
    @FindBy(xpath = "//section[@data-testid='country-selector-form']//button[@data-testid='save-country-button']")
    private WebElement saveCountryButton;
    public HomePage(WebDriver driver) {
        super(driver);
    }
    public boolean checkHeaderVisibility() {
        return header.isDisplayed();
    }
    public boolean checkMainBlockVisibility() {
        return mainBlock.isDisplayed();
    }
    public boolean checkFooterVisibility() {
        return footer.isDisplayed();
    }
    public boolean checkSearchFieldVisibility() {
        return searchField.isDisplayed();
    }
    public boolean checkAccountDropdownVisibility() {
        return accountDropdown.isDisplayed();
    }
    public boolean checkJoinButtonVisibility() {
        return joinButton.isDisplayed();
    }
    public boolean checkSignInButtonVisibility() {
        return signInButton.isDisplayed();
    }
    public boolean checkNameOfAccountVisibility() {
        return nameOfAccountOnTheAccountDropdown.isDisplayed();
    }
    public boolean checkWomenCategoryButtonVisibility() {
        return womenCategoryButton.isDisplayed();
    }
    public boolean checkMenCategoryButtonVisibility() {
        return menCategoryButton.isDisplayed();
    }
    public boolean checkViewAllShoesCategoryButton() {
        return viewAllShoesCategory.isDisplayed();
    }
    public boolean checkSignOutButtonVisibility() {
        return signOutButton.isDisplayed();
    }
    public boolean checkCartDropdownVisibility() {
        return cartDropdown.isDisplayed();
    }
    public boolean checkNumberOfProductsInCartVisibility() {
        return numberOfProductsInCart.isDisplayed();
    }
    public boolean checkCartDropdownPopUpVisibility() {
        return cartDropdownPopUp.isDisplayed();
    }
    public boolean checkEmptyCartButtonVisibility() {
        return emptyCartButton.isDisplayed();
    }
    public boolean checkInstagramLinkVisibility() {
        return instagramLink.isDisplayed();
    }
    public boolean checkSnapchatLinkVisibility() {
        return snapchatLink.isDisplayed();
    }
    public boolean checkPreferencesPopupVisibility() {
        return preferencesPopup.isDisplayed();
    }
    public void clickJoinButton() {
        joinButton.click();
    }
    public void clickSignInButton() {
        signInButton.click();
    }
    public void clickWomenCategoryButton() {
        womenCategoryButton.click();
    }
    public void clickMenCategoryButton() {
        menCategoryButton.click();
    }
    public void clickViewAllShoesCategory() {
        viewAllShoesCategory.click();
    }
    public void clickSignOutButton() {
        signOutButton.click();
    }
    public void clickDeleteItemsFromCartButton(int index) {
        deleteItemsFromCartButtonsList.get(index).click();
    }
    public void clickWishlistButton() {
        wishlistButton.click();
    }
    public void clickFacebookLink() {
        facebookLink.click();
    }
    public void clickInstagramLink() {
        instagramLink.click();
    }
    public void clickSnapchatLink() {
        snapchatLink.click();
    }
    public void inputTextToSearchField(String searchText) {
        searchField.clear();
        searchField.sendKeys(searchText, Keys.ENTER);
    }
    public void clickChangePreferencesButton() {
        changePreferencesButton.click();
    }
    public void clickSaveCountryButton() {
        saveCountryButton.click();
    }
    public WebElement getAccountDropdown() {
        return accountDropdown;
    }
    public WebElement getJoinButton() {
        return joinButton;
    }
    public WebElement getSignInButton() {
        return signInButton;
    }
    public WebElement getNameOfAccountOnTheAccountDropdown() {
        return nameOfAccountOnTheAccountDropdown;
    }
    public WebElement getWomenCategoryButton() {
        return womenCategoryButton;
    }
    public WebElement getMenCategoryButton() {
        return menCategoryButton;
    }
    public WebElement getShoesCategoryButton() {
        return shoesCategoryButton;
    }
    public WebElement getShopInDropdown() {
        return shopInDropdown;
    }
    public List<WebElement> getWomenSubcategoriesList() {
        return womenSubcategoriesList;
    }
    public List<WebElement> getWomenSubcategoriesDropdownsList() {
        return womenSubcategoriesDropdownsList;
    }
    public List<WebElement> getMenSubcategoriesDropdownsList() {
        return menSubcategoriesDropdownsList;
    }
    public WebElement getInstagramLink() {
        return instagramLink;
    }
    public WebElement getSnapchatLink() {
        return snapchatLink;
    }
    public List<WebElement> getMenSubcategoriesList() {
        return menSubcategoriesList;
    }
    public WebElement getViewAllShoesCategory() {
        return viewAllShoesCategory;
    }
    public WebElement getSignOutButton() {
        return signOutButton;
    }
    public WebElement getNumberOfProductsInCart() {
        return numberOfProductsInCart;
    }
    public WebElement getCartDropdown() {
        return cartDropdown;
    }
    public WebElement getCartDropdownPopUp() {
        return cartDropdownPopUp;
    }
    public List<WebElement> getDeleteItemsFromCartButtonsList() {
        return deleteItemsFromCartButtonsList;
    }
    public WebElement getEmptyCartButton() {
        return emptyCartButton;
    }
    public WebElement getPreferencesPopup() {
        return preferencesPopup;
    }
    public WebElement getSaveCountryButton() {
        return saveCountryButton;
    }
}

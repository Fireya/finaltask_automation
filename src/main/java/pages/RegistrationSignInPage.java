package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegistrationSignInPage extends BasePage {
    @FindBy(xpath = "//input[@alt='Email']")
    private WebElement emailFieldOnJoinPage;
    @FindBy(xpath = "//input[@id='FirstName']")
    private WebElement firstNameField;
    @FindBy(xpath = "//input[@id='LastName']")
    private WebElement lastNameField;
    @FindBy(xpath = "//input[@id='Password']")
    private WebElement passwordField;
    @FindBy(xpath = "//span[@id='Email-error']")
    private WebElement emailErrorMessageOnJoinPage;
    @FindBy(xpath = "//span[@id='FirstName-error']")
    private WebElement firstNameErrorMessageOnJoinPage;
    @FindBy(xpath = "//span[@id='LastName-error']")
    private WebElement lastNameErrorMessageOnJoinPage;
    @FindBy(xpath = "//span[@id='Password-error']")
    private WebElement passwordErrorMessageOnJoinPage;
    @FindBy(xpath = "//input[@id='EmailAddress']")
    private WebElement emailFieldOnSignInPage;
    @FindBy(xpath = "//input[@id='signin']")
    private WebElement signInButton;
    @FindBy(xpath = "//span[@id='EmailAddress-error']")
    private WebElement emailErrorMessageOnSignInPage;
    @FindBy(xpath = "//li[@id='loginErrorMessage']")
    private WebElement loginErrorMessage;

    public RegistrationSignInPage(WebDriver driver) {
        super(driver);
    }
    public WebElement getEmailFieldOnJoinPage() {
        return emailFieldOnJoinPage;
    }
    public WebElement getEmailFieldOnSignInPage() {
        return emailFieldOnSignInPage;
    }
    public WebElement getFirstNameField() {
        return firstNameField;
    }
    public WebElement getLastNameField() {
        return lastNameField;
    }
    public WebElement getPasswordField() {
        return passwordField;
    }
    public boolean isEmailErrorMessageOnJoinPageVisible() {
        return emailErrorMessageOnJoinPage.isDisplayed();
    }
    public boolean isFirstNameErrorMessageOnJoinPageVisible() {
        return firstNameErrorMessageOnJoinPage.isDisplayed();
    }
    public boolean isLastNameErrorMessageOnJoinPageVisible() {
        return lastNameErrorMessageOnJoinPage.isDisplayed();
    }
    public boolean isPasswordErrorMessageOnJoinPageVisible() {
        return passwordErrorMessageOnJoinPage.isDisplayed();
    }
    public boolean isEmailErrorMessageOnSignInPageVisible() {
        return emailErrorMessageOnSignInPage.isDisplayed();
    }
    public void clickSignInButton() {
        signInButton.click();
    }
}

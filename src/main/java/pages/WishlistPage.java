package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WishlistPage extends BasePage {
    @FindBy(xpath = "//div[contains(@class, 'itemCount')]")
    private WebElement itemsCount;
    public WishlistPage(WebDriver driver) {
        super(driver);
    }
    public boolean checkItemsCountVisible() {
        return itemsCount.isDisplayed();
    }
    public WebElement getItemsCount() {
        return itemsCount;
    }
}

Feature: Products functionality tests
  As a user
  I want to test functionality associated with products
  So that I can be sure that functionality associated with products works correctly on the site

  Scenario Outline: Check searching of products
    Given User opens '<homePage>' page
    And User checks visibility of header
    And User checks visibility of search field
    When User  inputs '<searchText>' text to Search field
    And User checks that url contains search '<expectedText>' text
    Then User checks that products contains search '<searchText>' text

    Examples:
      |homePage             |searchText       |expectedText        |
      |https://www.asos.com/|platform sneakers|?q=platform+sneakers|
      |https://www.asos.com/|floral dress     |?q=floral+dress     |
      |https://www.asos.com/|long coat        |?q=long+coat        |

  Scenario Outline: Check main actions with cart
    Given User opens '<homePage>' page
    And User checks visibility of header
    When User clicks on Women category button
    And User moves pointer to the Shoes button
    And User clicks on View All shoes category button
    And User adds <numberOfProduct> product to the cart
    Then User checks number of items in cart and compares to '<expectedNumberOfProduct>'
    And User checks visibility of header
    And User moves pointer to the cart dropdown
    And User clears up a cart and checks number of items in cart

    Examples:
      |homePage             |numberOfProduct|expectedNumberOfProduct|
      |https://www.asos.com/|1              |1                      |
      |https://www.asos.com/|2              |2                      |

  Scenario Outline: Check adding products to wishlist
    Given User opens '<homePage>' page
    And User checks visibility of header
    When User clicks on Women category button
    And User moves pointer to the Shoes button
    And User clicks on View All shoes category button
    Then User adds '<numberOfProduct>' product to the wishlist
    And User clicks on Wishlist button
    And User checks quantity '<numberOfProduct>' of products in wishlist

    Examples:
      |homePage             |numberOfProduct|
      |https://www.asos.com/|1              |
      |https://www.asos.com/|2              |

  Scenario Outline: Check correct work of filters
    Given User opens '<homePage>' page
    And User checks visibility of header
    When User clicks on Women category button
    And User moves pointer to the Shoes button
    And User clicks on View All shoes category button
    And User checks visibility of Brand and Price filters
    And User clicks and chooses Brand '<brand>' and Price filters parameters
    Then User checks that products corresponds to brand '<brand>' name and in low to high order

    Examples:
      |homePage             |brand      |
      |https://www.asos.com/|Dr Martens |